let courses = [];

postCourse = (id,name,description,price,isActive) => {
	courses.push({
		id: id,
		name: name,
		description: description,
		price: price,
		isActive: isActive

/*
	Arrow function syntax

	let/const sampleName = (parameter) = {
		//code block
	}

	let/const sampleName = (parameter) = {
		//code block
	}

	//only do this kind of arrow function if the code block is one-line
	//only omit the curly brace {} if code block is one-line
*/
	})
	return alert(`You have created ${name}. The price is ${price}`)

}
postCourse("course-1","PHP","A popular programming language","2500",true)
postCourse("course-2","Javascript","Another popular programming language","3000",true)
postCourse("course-3","CSS","A style sheet language","3000",true)
console.log(courses)

get


getSingleCourse = (idInput) => {
	//find(). forEach() and map() all need an anonymous function
	//anonymous functions are function with no names which means you cannot call/invoke them by themselves
	//anonymous functions are usually passed as arguments to other function or methods
	//finc() will repeat/iterate/loop over every item in array and run the anonymous function per item
	//the anonymous function will be able to receive the current item being looped

	let foundCourse = courses.find((course)=>{
		return course.id === idInput
		//the parameter in the anonymous function (course) is the current item being looped by the find() method
		//if the anonymous function in find(), returns true then the find() method will stop and return the current item being looped.
	})
	if (foundCourse !== undefined){
		console.log(foundCourse)
		//foundCourse will contain the found item
		//if no item was found or no item returned true in find(). find() will return undefined
	}
}

SingleCourse("course-1");
getSingleCourse("course-2");
getSingleCourse("course-3");


deleteCourse = () => {
	courses.pop()
}
deleteCourse();
console.log(courses)